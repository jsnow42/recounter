// package filedb - implements a file database.

package filedb

import (
	"encoding/gob"
	"io"
	"time"

	"os"
)

// FileDB - type representing a file database.
type FileDB struct {
	*os.File
}

// Load - loads previously saved events from the file.
func (db *FileDB) Load() ([]time.Time, error) {
	events := make([]time.Time, 0)
	if _, err := db.File.Seek(0, 0); err != nil {
		return events, err
	}
	err := gob.NewDecoder(db.File).Decode(&events)
	if err != nil && err != io.EOF {
		return events, err
	}
	return events, nil
}

// Save - saves events to the file.
func (db *FileDB) Save(events []time.Time) error {
	if err := db.File.Truncate(0); err != nil {
		return err
	}
	if _, err := db.File.Seek(0, 0); err != nil {
		return err
	}
	if err := gob.NewEncoder(db.File).Encode(events); err != nil {
		return err
	}
	return db.File.Sync()
}

// Close - closes fileDB.
func (db *FileDB) Close() error {
	if err := db.File.Sync(); err != nil {
		return err
	}
	return db.File.Close()
}

// New - creates a new instance of FileDB.
func New(filePath string) (*FileDB, error) {
	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return &FileDB{}, err
	}
	return &FileDB{File: file}, nil
}
