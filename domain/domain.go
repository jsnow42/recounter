// package domain - provides core interfaces `Counter`, `DB`, `Server', `Limiter`.

package domain

import (
	"net/http"
	"time"
)

// Server - a server that on each request responds with a counter of the total number of requests received during the
// given time interval (moving window). Server internally uses interface `Counter` to count the number of occurred
// events (in this case an event is a single request) and `DB` interface to store recorded events.
type Server interface {
	Serve(pattern string) error
	Backup() error
}

// Counter - counts the number of events occurred during the given time interval (mowing window).
type Counter interface {
	CreateEvent()
	CountEvents() int
	AddEvents(events []time.Time)
	AllEvents() (events []time.Time)
}

// DB - stores recorded events.
type DB interface {
	Load() (events []time.Time, err error)
	Save(events []time.Time) error
	Close() error
}

// Limiter - checks requests limits.
type Limiter interface {
	CheckLimits(req *http.Request) (Response, error)
}

// IP limits options.
const (
	LimitBySource = iota + 1 // IP is taken from the req.RemoteAddr field.
	LimitByHeader            // IP is taken from the request header.
)

// Response - response for limits.
type Response struct {
	IP               string // IP of a request
	WithinLimits     bool
	Period           time.Duration // counting period
	NumberOfRequests int
}
