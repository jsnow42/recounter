// package server - implements an HTTP server that on each request responds with a counter of the total number of
// requests received during the given time interval (moving window).

package server

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"gitlab.com/ayzu/recounter/limiter"

	"gitlab.com/ayzu/recounter/domain"
)

// Server - represents a server that on each request responds with a counter of the total number of requests received
// during the  given time interval (moving window).
type Server struct {
	domain.DB
	*http.Server

	counterMutex *sync.Mutex
	domain.Counter

	*limiter.Limiter
}

// withCounting - middleware that writes the number of the received requests to http.ResponseWriter.
func (server *Server) withCounting(baseHandler http.HandlerFunc) http.HandlerFunc {
	type Response struct {
		ReceivedRequests int `json:"received_requests"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() == "/favicon.ico" {
			baseHandler.ServeHTTP(w, r)
			return
		}
		server.counterMutex.Lock()
		server.Counter.CreateEvent()
		count := server.Counter.CountEvents()
		server.counterMutex.Unlock()

		limiterResp, err := server.Limiter.CheckLimits(r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, err)
			return
		}
		if !limiterResp.WithinLimits {
			w.WriteHeader(http.StatusTooManyRequests)
			fmt.Fprintf(w, "%+v", limiterResp)
			return
		}

		err = json.NewEncoder(w).Encode(Response{ReceivedRequests: count})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, err)

		} else {
			w.Header().Set("Content-type", "application/json")
			fmt.Fprintf(w, "%+v", limiterResp)
			baseHandler.ServeHTTP(w, r)
		}

	}
}

// baseHandler - final handler in the chain of handlers.
func baseHandler(_ http.ResponseWriter, _ *http.Request) {
	// some business logic goes here.
}

// Backup - saves collected events to the database.
func (server *Server) Backup() error {
	server.counterMutex.Lock()
	events := server.Counter.AllEvents()
	server.counterMutex.Unlock()
	return server.DB.Save(events)
}

// Serve - processes incoming HTTP requests.
func (server *Server) Serve(pattern string) error {
	mux := http.NewServeMux()
	mux.HandleFunc(pattern, server.withCounting(baseHandler))
	server.Handler = mux
	return server.Server.ListenAndServe()
}

// Stop - shutdowns the server.
func (server *Server) Stop() error {
	if err := server.Backup(); err != nil {
		return err
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	return server.Shutdown(ctx)
}

// New - creates a new instance of `Server`.
func New(counter domain.Counter, lim *limiter.Limiter, db domain.DB, addr, port string) (s Server, err error) {
	events, err := db.Load()
	if err != nil {
		return s, err
	}
	counter.AddEvents(events)
	return Server{
		DB: db,
		Server: &http.Server{
			Addr:         addr + ":" + port,
			ReadTimeout:  time.Second * 2,
			WriteTimeout: time.Second * 2,
		},

		counterMutex: &sync.Mutex{},
		Counter:      counter,

		Limiter: lim,
	}, nil
}
